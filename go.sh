#!/bin/bash
set -e
docker build -t webdriver .
docker run -e GITLAB_USER -e GITLAB_PASSWORD webdriver