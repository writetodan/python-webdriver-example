import os

import pytest

from test.pages.gitlab import GitlabLoginPage


def username():
    return os.environ['GITLAB_USER']

def password():
    return os.environ['GITLAB_PASSWORD']


def test_gitlab_is_the_fave_project_on_gitlab_dot_com_with_sign_in():
    first_page = GitlabLoginPage().visit().login(username(), password())

    projects_page = first_page.explore_projects()

    most_starred = projects_page.switch_to_most_stars().first_project()

    assert 'GitLab' in most_starred.project_title



