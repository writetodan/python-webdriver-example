import os
import atexit

from subprocess import run, DEVNULL

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import chromedriver_binary


def has_display():
    try:
        return run('xhost', stdout=DEVNULL, stderr=DEVNULL).returncode == 0
    except FileNotFoundError:
        return False


class ChromeBrowser:
    def __init__(self):
        self.chrome = None
        
    @property
    def webdriver(self):
        if self.chrome is None:
            options = Options()
            if self.run_headless():
                options.headless = True

            options.add_argument('--no-sandbox')
            chrome = webdriver.Chrome(options=options)
            atexit.register(lambda: chrome.quit())
            self.chrome = chrome

        return self.chrome    
    
    def run_headless(self):
        return os.environ.get('HEADLESS', None) or not has_display()

