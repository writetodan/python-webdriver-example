from .browser import ChromeBrowser

class GitlabLoginPage:
    def __init__(self, webdriver=ChromeBrowser().webdriver):
        self.webdriver = webdriver

    def visit(self):
        self.webdriver.get('https://gitlab.com/users/sign_in')
        return self

    def login(self, username, password):
        login_form = self.webdriver.find_element_by_css_selector('form.new_user')
        login_form.find_element_by_name('user[login]').send_keys(username)
        login_form.find_element_by_name('user[password]').send_keys(password)
        login_form.submit()

        # if not pushed project yet
        # this would be modelled on a different page, e.g. NoProjectsWelcomePage
        # adding an is_current() method to each possible page would let us
        # query those possible pages to determine which flow to go down (and which)
        # page object to return after submit()
        #
        # welcome = chrome.find_element_by_css_selector('.blank-state-welcome h2')
        # print(welcome.text)
        # assert welcome.text == 'Welcome to GitLab'
        # chrome.find_element_by_partial_link_text('Explore public projects').click()

        return ProjectsListPage(self.webdriver)


    @property
    def chrome(self):
        return self.webdriver


class ProjectsListPage:
    def __init__(self, webdriver):
        self.webdriver = webdriver

    def explore_projects(self):
        self.webdriver.find_element_by_partial_link_text('Explore projects').click()
        return self

    def switch_to_most_stars(self):
        self.webdriver.find_element_by_partial_link_text('Most stars').click()
        return self

    def first_project(self):
        project_element = self.webdriver.find_element_by_css_selector('.project-row .project-title')
        return ProjectListItem(project_element)


class ProjectListItem:
    def __init__(self, web_element):
        self.element = web_element

    @property
    def project_title(self):
        return self.element.text