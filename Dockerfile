FROM centos/python-36-centos7
RUN curl https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm --output chrome.rpm
RUN whoami
USER root
RUN yum install -y ./chrome.rpm
ADD requirements.txt .
RUN pip install -r requirements.txt
RUN python -c 'import chromedriver_binary'
RUN mkdir -p ./test/pages
ADD test/*.py ./test/
ADD test/pages/*.py ./test/pages/
CMD python -m pytest