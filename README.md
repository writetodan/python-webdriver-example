## python-webdriver

Example python driving headless chrome, with page objects.

## The example

This example signs into gitlab.com and checks the most popular project.

As such it needs env vars `GITLAB_USER` and `GITLAB_PASSWORD` setting in your environment first, either with `export` or passing on command line

## Setting username and password

TIP The leading space before password export is to prevent storage of your password in history.  If leading space doesn't skip the save to history by default for you, check out HISTCONTROL in bash or HIST_IGNORE_SPACE in zsh.

```
export GITLAB_USER=myusername
 export GITLAB_PASSWORD=mypassword 
```

## Running

To run in docker container (headless, i.e. not visible):
```
./go.sh
```

To run locally on your host computer (with showing browser if running xhost), install dependencies and run:
```
./local-go.sh
```


## Installing dependencies

Needs: python 3 with pip, compatible google chrome.  Chromedriver is installed by python itself when run.py is run.